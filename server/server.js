const express = require('express');

const socketIO = require("socket.io")

const http = require("http");

const path = require('path');

const { Usuarios } = require("./classes/usuario");

const { crearMensaje } = require("./utilidades/utilidades");

const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

let io = socketIO(server);

const usuarios = new Usuarios();

io.on("connection", (client) => {
    console.log("Usuario conectado");

    client.on("entrarChat", (data, callback) =>{
        
        if (!data.nombre || !data.sala) {
            return callback({
                error: true,
                mensaje: "El nombre es necesario",
            });
        }

        client.join(data.sala);

        usuarios.agregarPersona(client.id, data.nombre, data.sala);
        
        client.broadcast
            .to(data.sala)
            .emit("listarPersonas", usuarios.getPersonaPorSala(data.sala));

        io.sockets.emit('listaPersonas', usuarios.getPersonas());

        callback(usuarios.getPersonaPorSala(data.sala));
    });

    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id)

        let mensaje = crearMensaje(persona.nombre, data.mensaje);

        client.broadcast.emit("crearMensaje", mensaje);
        
    })

    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

        client.broadcast.to(personaBorrada.sala).emit("crearMensaje", {
            nombre: "Administrador",
            mensaje: `${personaBorrada.nombre} salio`,
            fecha: new Date().getTime()
        });

        client.broadcast.to(personaBorrada.sala).emit("listaPersonas", usuarios.getPersonas());
    })

    client.on("mensajePrivado", (data) => {
        let persona = usuarios.getPersona(client.id);
        client.broadcast.to(data.para).emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
    })
});

server.listen(port, (err) => {
    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);
});